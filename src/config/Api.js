import axios from 'axios';

import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import Session from '.././Session/Session';

    const instance =
        axios.create({
            baseURL: `http://18.189.31.121:8080/taskmanagement-0.0.1-SNAPSHOT/api/`,
            headers: {
                'Content-Type': 'application/json',
            }
        });
        Session.getSession("token")
        .then((column)=>
        {
            instance.defaults.headers.common['Authorization'] = 'Bearer '+JSON.parse(column);        
        })
export default instance;
