import React from 'react';
import { Button, Platform, Image, View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import List from '.././components/List';
import Home from '.././components/Home';
import ModalScreen from '.././components/Modal';
import Detail from '.././components/Detail';
import Login from '.././components/Login';
import Daftar from '.././components/Daftar';

const MainStack = createStackNavigator(
    {
      List: {
        screen: List,
      },
      Home : 
      { screen : Home },
      Login :
      {screen : Login},
      Daftar :
      {screen : Daftar}
    //   Detail : 
    //   {screen D}
    },
    {
      initialRouteName: 'Login',
    //   navigationOptions: {
    //     headerShown: false,
    //   }
      defaultNavigationOptions: {
        headerShown: false
    //     headerStyle: {
    //       backgroundColor: '#f4511e',
    //     },
    //     headerTintColor: '#fff',
    //     headerTitleStyle: {
    //       fontWeight: 'bold',
    //     },
      },
    }
  );
  
  const RootStack = createStackNavigator(
    {
        
      Main: {
        screen: MainStack,
      },
      MyModal: {
        screen: ModalScreen,
      }
    },
    {
      mode: 'modal',
      headerMode: 'none',
      navigationOptions: {
        headerShown: false,
      }
    }
  );
  
  const AppContainer = createAppContainer(RootStack);

  export default AppContainer;