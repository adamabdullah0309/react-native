import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import Modal from './Modal'

export default class Home extends Component{
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};
    
        return {
          headerLeft: () => (
            <Button
              onPress={() => navigation.navigate('MyModal')}
              title="Info"
              color={Platform.OS === 'ios' ? '#fff' : null}
            />
          ),
          headerRight: () => (
            <Button onPress={params.increaseCount} title="+1" color={Platform.OS === 'ios' ? "#fff" : null} />
          ),
        };
      };
    
      componentWillMount() {
        this.props.navigation.setParams({ increaseCount: this._increaseCount });
      }
    
      state = {
        count: 0,
      };
    
      _increaseCount = () => {
        this.setState({ count: this.state.count + 1 });
      };
    
      render() {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>
            <Text>Count: {this.state.count}</Text>
            <Button
              title="Go to Details"
              onPress={() => {
                /* 1. Navigate to the Details route with params */
                this.props.navigation.navigate('Details', {
                  itemId: 86,
                  otherParam: 'First Details',
                });
              }}
            />
          </View>
        );
      }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
