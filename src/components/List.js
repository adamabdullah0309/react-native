import React, { Component } from 'react';
import { Button, Platform, Image, View, Text, SafeAreaView, FlatList, StyleSheet } from 'react-native';
import LoginService from '.././services/LoginServices'
import Constants from 'expo-constants';

export default class List extends Component {
    constructor()
    {
      super()
      this.state =
      {
        data : []
      }
    }


    static navigationOptions = ({ navigation }) => {
            return {
              title: navigation.getParam('otherParam', 'A Nested Details Screen'),
            };
          };
    list = () =>
    {
        LoginService.getListEmployee({
            company_id :1
          })
          .then((resp) => 
          {
            let data = resp.data;
            // console.log("halo");
            // console.log("data = ",data.responseData[0].alamat);
            // this.setState({data : data.responseData});
            let dataResp = [];
            for (var i = 0; i < data.responseData.length; i++) {
              let obj = data.responseData[i]
              dataResp.push({
                key: i,
                kdkaryawan : obj.kdkaryawan,
                nmkaryawan : obj.nmkaryawan
              })

            }
            this.setState({data : dataResp});
          },
          (cause)=>
          {
              alert("Something wrong");
            // console.log("error", cause);
          });
    }

    componentDidMount()
  {
    this.list();
  }




    render() {
      function Item({ title }) {
        return (
          <View style={styles.item}>
            <Text style={styles.title}>{title}</Text>
          </View>
        );
      }

      // console.log(this.state.data[0]);
      const data2 = this.state.data;
      const DATA = [
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
          title: 'First Item1',
        },
        {
          id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
          title: 'Second Item',
        },
        {
          id: '58694a0f-3da1-471f-bd96-145571e29d72',
          title: 'Third Item',
        },
      ];
      return(
        <SafeAreaView style={styles.container}>
          <FlatList
            data={data2}
            renderItem={({ item }) => <Item title={item.kdkaryawan}  />}
            keyExtractor={item => item.key}
          />
        </SafeAreaView>
      )
    //     console.log(this.state.data[1]);
    //     const { navigation } = this.props;
    //     return (
    //       <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    //         <Text>
    //            username: {JSON.stringify(navigation.getParam('username', 'username-user'))}
    //            password: {JSON.stringify(navigation.getParam('password', 'password-user'))}
    //      </Text>
    //         <Button
    //           onPress={() => this.props.navigation.goBack()}
    //           title="Dismiss"
    //         />
    //       </View>
    //     );
      }

      
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: Constants.statusBarHeight,
    },
    item: {
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    title: {
      fontSize: 32,
    },
  });