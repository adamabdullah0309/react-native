import React, { Component } from 'react';
import { Button, Platform, Image, View, Text } from 'react-native';


export default class Modal extends Component {
    render() {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 30 }}>Ada Modal</Text>
            <Button
              onPress={() => this.props.navigation.goBack()}
              title="Dismiss"
            />
          </View>
        );
      }
  }