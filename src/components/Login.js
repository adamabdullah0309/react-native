import React, {Component} from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, Button, TextInput, Dimensions, TouchableOpacity } from 'react-native';
import bgImage from '.././images/background.png'
// import logo from './images/logo.png'
import Icon from 'react-native-vector-icons/Ionicons'
import LoginService from '.././services/LoginServices'
import Session from '.././Session/Session'

const {width : WIDTH} = Dimensions.get('window')
export default class App extends Component {

  // constructor()
  // {
  //   this.state = {
  //     showPass : true,
  //     press : false
  //   }
  // }
  constructor()
  {
    super()
    this.state = 
    {
      showPass : true,
      press : false,
      username : "",
      password : ""
    }
  }
    login = () => 
    {
        let username = this.state.username;
        let password = this.state.password;
        LoginService.login({
            username : username,
            password : password
          })
          .then((resp) => 
          {
            
            let token = resp.data.result;
            console.log("sdfsdf",token); 
            Session.setSession(token);
            this.props.navigation.navigate('List');
          },
          (cause)=>
          {
              alert("Something wrong");
          });
    }

    daftar = () =>
    {
        this.props.navigation.navigate('Daftar');
    }

  showPass = () =>
  {
    if(this.state.press == false)
    {
      this.setState({showPass : false , press : true})
    }
    else
    {
      this.setState({showPass : true, press : false})
    }
  }

  render()
  {
    const { navigation } = this.props;
    return (
      <ImageBackground source={bgImage} style={styles.backgroundContainer}>
        <View style={styles.logoContainer}>
          {/* <Image source={logo} style={styles.logo} ></Image> */}
          <Text style={styles.logoText}>Login</Text>
        </View>
        <View style={styles.inputContainer}>
          <Icon name={'ios-person'} size={28} color={'rgba(255, 255, 255, 0.7)'} 
          style={styles.inputIcon}/>
          <TextInput 
            style = {styles.input}
            placeholder={'Username'} 
            onChangeText={(username) => this.setState({username})}
            value={this.state.username}
            placeholderTextColor={'rgba(255, 255, 255, 0.7)'}/>
        </View>

        <View style={styles.inputContainer}>
          <Icon name={'ios-lock'} size={28} color={'rgba(255, 255, 255, 0.7)'} 
          style={styles.inputIcon}/>
          <TextInput 
            style = {styles.input}
            placeholder={'Password'} 
            secureTextEntry = {this.state.showPass}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
            
            placeholderTextColor={'rgba(255, 255, 255, 0.7)'}/>
          <TouchableOpacity style={styles.btneyes} onPress={this.showPass.bind(this)}>
            <Icon name ={this.state.press == false ? 'ios-eye' : 'ios-eye-off'} size={26} color={'rgba(255, 255, 255, 0.7)'} 
             />
          </TouchableOpacity>
        </View>
        
        <TouchableOpacity style={styles.btnLogin} onPress={this.login} >
          <Text style={styles.text}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.btnLogin} onPress={this.daftar} >
          <Text style={styles.text}>Daftar</Text>
        </TouchableOpacity>
      </ImageBackground>
    );  
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundContainer :
  {
    flex : 1,
    justifyContent : 'center',
    width : null,
    height: null,
    alignItems : 'center'
  },
  logoContainer :
  {
    alignItems : 'center',
    marginBottom : 50

  },
  logo :
  {
    width : 120,
    height : 120
  },
  logoText :
  {
    color : 'white',
    fontSize : 20,
    fontWeight : '500',
    marginTop : 10,
    opacity : 0.5
  },
  inputContainer : 
  {
    marginTop : 10,
    
  },
  input :
  {
    width : WIDTH - 55,
    height : 45,
    borderRadius : 45,
    fontSize : 16,
    paddingLeft : 45,
    backgroundColor : 'rgba(0,0,0,0.35)',
    color : 'rgba(255,255,255,0.7)',
    marginHorizontal : 25
  },
  inputIcon :
  {
    position : 'absolute',
    top : 8,
    left : 37
  },
  btneyes :
  {
    position : 'absolute',
    top : 8,
    right : 37
  },
  btnLogin :
  {
    width : WIDTH - 55,
    
    height : 45,
    borderRadius : 45,
    fontSize : 16,
    backgroundColor : '#432577',
    justifyContent : 'center',
    marginTop :20
  },
  text : 
  {
    color : 'rgba(255, 255, 255,0.7)',
    fontSize : 16,
    textAlign : 'center'
  },
});


































































