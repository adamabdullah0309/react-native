import React,{Fragment, useEffect} from 'react';
import { StyleSheet, Text, View } from 'react-native';


export default class SplashScreen extends React.Component {
    render() {
    //   const viewStyles = [
    //     styles.container,
    //     { backgroundColor: 'orange' }
    //   ];
    //   const textStyles = {
    //     color: 'white',
    //     fontSize: 40,
    //     fontWeight: 'bold'
    //   };
  
      return (
        <View style={style.viewStyles}>
          <Text style={style.textStyles}>
            Splash Screen
          </Text>
        </View>
      );
    }
  }

  const style = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    textStyles : 
    {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold'
    },
    viewStyles : 
    {
        flex : 1,
        justifyContent : 'center',
        backgroundColor: '#fff',
        width : null,
        height: null,
        alignItems : 'center'
    }
});