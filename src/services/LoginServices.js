// import API from './../config/Api';
import axios from 'axios';
import API from '.././config/Api';

export default {
	
	getListEmployee(request) {
		return API.post(`TMGetListKaryawan/a`, request)
    },
    login(request) {
		return axios.post(`http://18.189.31.121:8080/taskmanagement-0.0.1-SNAPSHOT/api/login`, request)
	},
	saveEmployee(request) {
		return API.post(`TMSaveKaryawan`, request)
	},
	getDetailEmployee(request) {
		return API.post(`TMGetDetailKaryawan`, request)
	},
	updateEmployee(request) {
		return API.post(`TMUpdateKaryawan`, request)
	},
}
